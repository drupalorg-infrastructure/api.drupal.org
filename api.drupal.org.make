core = 7.x
api = 2
projects[drupal][version] = "7.96"


projects[api][version] = "2.4"

projects[apidrupalorg][version] = "1.x-dev"
projects[apidrupalorg][download][revision] = "90f2398"

projects[ctools][version] = "1.15"

projects[codefilter][version] = "1.x-dev"
; <code><em>HTML in code</em></code> is not properly escaped with Prism
; https://www.drupal.org/node/2694791
projects[codefilter][download][revision] = "c0c7ea2"

projects[comment_fragment][version] = "1.2"

projects[date][version] = "2.10"

projects[diff][version] = "3.4"

projects[entity][version] = "1.9"

projects[facetapi][version] = "1.5"

projects[features][version] = "2.11"

projects[field_extrawidgets][version] = "1.1"

projects[google_analytics][version] = "2.6"

projects[link][version] = "1.11"

projects[logintoboggan][version] = "1.5"

projects[redirect][version] = "1.0-rc3"

projects[views_bulk_operations][version] = "3.5"

projects[views][version] = "3.24"

projects[views_data_export][version] = "3.2"


; Libraries

; Required by codefilter_prism
libraries[prism][type] = "libraries"
libraries[prism][download][type] = "get"
libraries[prism][download][url] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/get/1af9ec7f8967.zip"
libraries[prism][download][subtree] = "drupalorg-infrastructure-drupal.org-sites-common-1af9ec7f8967/prism"


;; Common for Drupal.org D7 sites.

includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
